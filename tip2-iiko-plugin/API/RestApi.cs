﻿using RestSharp;
using System.Configuration;

namespace Resto.Front.Api.Tip2IikoPlugin
{
    class RestApi
    {
        private RestClient Client;
        private readonly string BaseUrl;
        public RestApi()
        {
            this.BaseUrl = ConfigurationManager.AppSettings["URL"];
            this.Client = new RestClient(BaseUrl);
            Client.Timeout = 5000;
            Client.ReadWriteTimeout = 5000;
        }
        private IRestRequest CreateRequest(string url, Method method)
        {
            var request = new RestRequest(BaseUrl + url, method);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            return request;
        }

        public IRestResponse SendGet(string url, string key, string name)
        {
            var request = CreateRequest(url + '/', Method.GET);
            request.AddQueryParameter("key", key);
            if (!string.IsNullOrEmpty(name))
                request.AddQueryParameter("name", name);
            var response = Client.Execute(request, Method.GET);
            return response;
        }

    }
}
