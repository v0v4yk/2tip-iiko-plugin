﻿using Newtonsoft.Json;
using Resto.Front.Api.Tip2IikoPlugin.Models;
using Resto.Front.Api.V6;
using Resto.Front.Api.V6.Data.Common;
using Resto.Front.Api.V6.Data.Security;
using System;
using System.Collections.Generic;

namespace Resto.Front.Api.Tip2IikoPlugin.Observers
{
    class UserObserver : IObserver<EntityChangedEventArgs<IUser>>
    {
        private RestApi client = new RestApi();
        private DBManager DbManager;
        public UserObserver(DBManager manager)
        {
            this.DbManager = manager;
        }
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(EntityChangedEventArgs<IUser> value)
        {
            IUser user = value.Entity;
            Response response;
            IDictionary<string, string> dict = new Dictionary<string, string>();
            switch (value.EventType)
            {
                case EntityEventType.Created:
                case EntityEventType.Updated:
                    response = JsonConvert.DeserializeObject<Response>(client.SendGet("iiko", DbManager.GetKeyNumber(), user.Name).Content);
                    if (response == null || response.success == false)
                    {
                        PluginContext.Log.Error(response == null ? "Can't connect to 2tip.ru" : response.error);
                        break;
                    }
                    dict.Add("IikoName", user.Name);
                    dict.Add("Tip2Url", response.url);
                    dict.Add("Alias", response.alias);
                    DbManager.AddOrUpdateClient(user.Id.ToString(), dict);
                    break;
                case EntityEventType.Removed:
                    DbManager.DeleteClient(user.Id.ToString(), user.Name);
                    break;                    
            }
        }
    }
}
