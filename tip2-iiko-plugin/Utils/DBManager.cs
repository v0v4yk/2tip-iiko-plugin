﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Data;
using Resto.Front.Api.V6;
using Resto.Front.Api.Tip2IikoPlugin.Models;
using System.Configuration;

namespace Resto.Front.Api.Tip2IikoPlugin
{
    public class DBManager
    {
        public SQLiteConnection connection;
        public DBManager(string path)
        {
            string filename = Path.Combine(path, ConfigurationManager.AppSettings["DbFileName"]);
            if (!File.Exists(filename))
            {
                Directory.CreateDirectory(path);
                SQLiteConnection.CreateFile(filename);

            }
            string initWaiters = @"CREATE TABLE IF NOT EXISTS [Waiters](
                                    IikoId TEXT PRIMARY KEY,
                                    IikoName TEXT,
                                    Tip2Url TEXT,
                                    Alias TEXT
                                );";
            string initKeys = @"CREATE TABLE IF NOT EXISTS [Keys](
                                    key TEXT PRIMARY KEY,
                                    value TEXT
                                );";
            connection = new SQLiteConnection($"Data Source ={filename}");
            connection.ConnectionString = "Data Source = " + filename;
            connection.Open();

            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                command.CommandText = initWaiters;
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
                command.CommandText = initKeys;
                command.ExecuteNonQuery();
            }
        }
        private void execNonQuery(string sql, bool throwException = true)
        {
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                try
                {
                    command.CommandText = sql;
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SQLiteException e)
                {
                    PluginContext.Log.Error(sql);
                    PluginContext.Log.Error(e.Message);
                    if (throwException)
                        throw e;
                }
            }
        }
        private List<string[]> execQuery(string sql)
        {
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                SQLiteDataReader reader = command.ExecuteReader();
                List<string[]> result = new List<String[]>();
                while (reader.Read())
                {
                    string[] row = new String[reader.FieldCount];
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        row[i] = reader.GetValue(i).ToString();
                    }
                    result.Add(row);
                }
                return result;
            }
        }
        public void CreateKeyNumber()
        {
            PluginContext.Log.Info($"Insert validate key");
            string sqltext = @"INSERT INTO [Keys] VALUES('key', '')";
            execNonQuery(sqltext);
        }
        public void UpdateKeyNumber(string keyNumber)
        {
            PluginContext.Log.Info($"Change validate key {keyNumber}");
            string sqltext = $"Update [Keys] SET value = '{keyNumber}' WHERE [key] = 'key'";
            execNonQuery(sqltext);
        }
        public string GetKeyNumber()
        {
            PluginContext.Log.Info($"Get validate key");
            string sqltext = $"SELECT * FROM [Keys] WHERE [key] = 'key'";
            var key = execQuery(sqltext);
            if (key.Count > 0)
            {
                PluginContext.Log.Info($"Validate key value = {key[0][1]}");
                return key[0][1];
            }
            PluginContext.Log.Info("Validate key value = null");
            return null;
        }
        public void DeleteClient(string Id, string IikoName)
        {
            PluginContext.Log.Info($"Delete waiter {IikoName} (id = {Id}) from DB");
            string sqltext = $"DELETE FROM [Waiters] where IikoId = '{Id}'";
            execNonQuery(sqltext);
        }
        public void AddOrUpdateClient(string IikoId, IDictionary<string, string> dict)
        {
            PluginContext.Log.InfoFormat($"Update waiter {dict["IikoName"]} in DB", dict);
            string sqltext;
            Waiter waiter = GetWaiterFromDB(IikoId, dict["IikoName"]);
            if (waiter.IikoId == null)
            {
                sqltext = "INSERT INTO [Waiters] VALUES('{0}', '{1}', '{2}', '{3}')";
            }
            else
            {
                sqltext = "UPDATE [Waiters] SET IikoId = '{0}', IikoName = '{1}', Tip2Url = '{2}', `Alias` = '{3}' WHERE IikoId = '{0}'";
            }
            sqltext = string.Format(sqltext, IikoId, dict["IikoName"], dict["Tip2Url"], dict["Alias"]);
            execNonQuery(sqltext);
        }
        public Waiter GetWaiterFromDB(string iikoId, string IikoName)
        {
            PluginContext.Log.Info($"Get {IikoName} (id = {iikoId}) from DB");
            string sqlString = $"SELECT * FROM [Waiters] WHERE IikoId = '{iikoId}'";
            Waiter waiter = new Waiter();
            var list = execQuery(sqlString);
            if (list.Count > 0)
            {
                waiter.IikoId = list[0][0];
                waiter.IikoName = list[0][1];
                waiter.Tip2Url = list[0][2];
                waiter.Alias = list[0][3];
                PluginContext.Log.Info($"\nName {waiter.IikoName}\nId {waiter.IikoId}\n URL {waiter.Tip2Url}\nAlias {waiter.Alias}");
            }
            return waiter;
        }
        public void Dispose()
        {
            this.connection.Close();
        }
    }
}
