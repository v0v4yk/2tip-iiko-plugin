﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Resto.Front.Api.Tip2IikoPlugin.Views
{
    public partial class StatusWindow : UserControl
    {
        private delegate void UpdateTextDelegate(object value);
        private Mutex mutex;
        private Message message;
        public StatusWindow(string title, Message message, Mutex mutex)
        {
            this.mutex = mutex;
            this.message = message;
            
            this.Initialized += (s, e) => {
                this.message.PropertyChanged += Message_PropertyChanged;
            };
            InitializeComponent();
            this.label.Content = title;
            UpdateText(message.message);
        }
        private void UpdateText(object value)
        {
            if (string.Equals("Синхронизация завершена", value) && this.Parent != null)
            {
                Close(false);
            }
            else
            {
                messageBox.Text = value as string;
            }
        }
        private void Message_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            messageBox.Dispatcher.Invoke(new UpdateTextDelegate(this.UpdateText), e.PropertyName);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Close(bool byButton = true)
        {
            if (byButton)
            {
                mutex.WaitOne();
                this.message.PropertyChanged -= Message_PropertyChanged;
                (this.Parent as Window).Close();
                mutex.ReleaseMutex();
            }
            else
            {
                this.message.PropertyChanged -= Message_PropertyChanged;
                (this.Parent as Window).Close();
            }
        }
    }
    public class Message : INotifyPropertyChanged
    {
        public string messageValue;
        public string message
        {
            get { return messageValue; }
            set
            {
                if (value != messageValue)
                {
                    this.messageValue = value;
                    NotifyPropertyChanged(value);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
