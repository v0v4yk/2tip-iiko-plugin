﻿namespace Resto.Front.Api.Tip2IikoPlugin.Models
{
    class Response
    {
        public string alias { get; set; }
        public string url { get; set; }
        public string error { get; set; }
        public bool success { get; set; }
    }
}
