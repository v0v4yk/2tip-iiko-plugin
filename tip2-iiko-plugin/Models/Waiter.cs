﻿namespace Resto.Front.Api.Tip2IikoPlugin.Models
{
    public class Waiter
    {
        public string IikoId { get; set; }
        public string IikoName { get; set; }
        public string Tip2Url { get; set; }
        public string Alias { get; set; }
    }
}
