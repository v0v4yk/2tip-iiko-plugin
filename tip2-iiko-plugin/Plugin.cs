﻿using iiko.Keyboard;
using Newtonsoft.Json;
using Resto.Front.Api.Tip2IikoPlugin.Models;
using Resto.Front.Api.Tip2IikoPlugin.Views;
using Resto.Front.Api.V6;
using Resto.Front.Api.V6.Attributes;
using Resto.Front.Api.V6.Attributes.JetBrains;
using Resto.Front.Api.V6.Data.Cheques;
using Resto.Front.Api.V6.RemotingHelpers;
using Resto.Front.Api.V6.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Xml.Linq;

namespace Resto.Front.Api.Tip2IikoPlugin
{
    [UsedImplicitly]
    [PluginLicenseModuleId(21016318)]
    public class Plugin: IFrontPlugin
    {
        private RestApi client;
        public string keyNumber;
        private DBManager DbManager;
        Message message;
        private Mutex mutex= new Mutex();
        private bool closed;
        private void StatusWindowStart(string title, Mutex mutex)
        {
            StatusWindow statusWindow = new StatusWindow(title, message, mutex);
            Window window = new Window
            {

                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize,
                Content = statusWindow,
                Title = GetType().Name,
                Topmost = true,
                WindowStyle = WindowStyle.None,
                ShowActivated = true,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
            };
            window.Closed += (s, e) =>
            {
                closed = true;
                System.Windows.Threading.Dispatcher.ExitAllFrames();
            };
            window.ShowDialog();
            System.Windows.Threading.Dispatcher.Run();
        }
        private void syncButtonAction(IViewManager vm, IReceiptPrinter rp)
        {
            bool repeat = true;
            int count = 0, n = 0, success = 0;
            while (repeat)
            {
                PluginContext.Log.Info("Синхронизация");
                message = new Message { message = "Проверка ключа доступа" };
                PluginContext.Log.Info(message.message);
                Thread statusThread = new Thread(new ThreadStart(()=>StatusWindowStart("Статус", this.mutex)));
                statusThread.SetApartmentState(ApartmentState.STA);
                statusThread.Start();
                closed = false;
                string keyNumberDB = DbManager.GetKeyNumber();
                var response = JsonConvert.DeserializeObject<Response>(client.SendGet("iiko/validate", keyNumberDB, string.Empty).Content);
                if (response == null)
                {
                    mutex.WaitOne();
                    message.message = "Синхронизация завершена";
                    mutex.ReleaseMutex();
                    repeat = vm.ShowYesNoPopup("Ошибка соединения с сервером 2tip", "Повторить попытку?");
                    PluginContext.Log.Error("Can't connect to 2tip.ru");
                    continue;
                }
                PluginContext.Log.InfoFormat("Response from 2tip", response);
                if (response.success == true)
                {
                    repeat = false;
                    var users = PluginContext.Operations.GetUsers();
                    count = users.Count();
                    foreach (var user in users)
                    {
                        n++;
                        if (closed)
                        {
                            break;
                        }
                        mutex.WaitOne();
                        message.message = $"Поиск информации о пользователе ({n} из {count})";
                        PluginContext.Log.Info(message.message);
                        if (AddOrUpdateWaiterDB(user.Name, user.Id.ToString(), DbManager.GetWaiterFromDB(user.Id.ToString(), user.Name)))
                            success++;
                        mutex.ReleaseMutex();
                    }
                    PluginContext.Log.Info($"Синхронизация завершена. Найдено {success} из {count} пользователей");
                    if (!closed)
                    {
                        mutex.WaitOne();
                        message.message = "Синхронизация завершена";
                        mutex.ReleaseMutex();
                    }
                    vm.ShowOkPopup("Синхронизация завершена", $"Найдено {success} из {count} пользователей");
                }
                else
                {
                    mutex.WaitOne();
                    message.message = "Синхронизация завершена";
                    mutex.ReleaseMutex();
                    vm.ShowErrorPopup("Неверный ключ доступа");
                    keyNumber = UpdateKeyNumber();
                    repeat = keyNumber != null;
                }
            }
        }
        private bool AddOrUpdateWaiterDB(string name, string Id, Waiter waiterFromDb)
        {
            var response = JsonConvert.DeserializeObject<Response>(client.SendGet("iiko", DbManager.GetKeyNumber(), name).Content);
            PluginContext.Log.Info($"Response from 2tip \n\talias: {response.alias}, \n\turl: {response.url}, \n\tsuccess: {response.success}");
            if (string.IsNullOrEmpty(response.error))
            {
                if (!string.IsNullOrEmpty(response.url))
                {
                    IDictionary<string, string> dict = new Dictionary<string, string>
                    {
                        { "IikoName", name },
                        { "Tip2Url", response.url },
                        { "Alias", response.alias }
                    };
                    DbManager.AddOrUpdateClient(Id, dict);
                }
            }
            else
            {
                PluginContext.Log.Error($"{name} " + response.error);
            }
            return response.success;
        }
        private V6.Data.Print.Document GetQrDoc(Guid orderId)
        {
            var waiter = PluginContext.Operations.GetOrderById(orderId).Waiter;
            V6.Data.Print.Document qrDoc = new V6.Data.Print.Document();
            try
            {
                if (waiter != null)
                {
                    Waiter waiterFromDb = DbManager.GetWaiterFromDB(waiter.Id.ToString(), waiter.Name);
                    qrDoc.Markup = new XElement(Tags.Doc);
                    qrDoc.Markup.Add(new XElement(Tags.Line, "_"));
                    if (!string.IsNullOrEmpty(waiterFromDb.Tip2Url))
                    {
                        qrDoc.Markup.Add(new XElement(Tags.Center, "Чтобы оставить чаевые безналичными"));
                        qrDoc.Markup.Add(new XElement(Tags.Center, " наведите камеру телефона на QR-код."));
                        qrDoc.Markup.Add(new XElement(Tags.QRCode, waiterFromDb.Tip2Url));
                        if (!string.IsNullOrEmpty(waiterFromDb.Alias))
                        {
                            qrDoc.Markup.Add(new XElement(Tags.Center, "Если не удалось отсканировать QR-код,"));
                            qrDoc.Markup.Add(new XElement(Tags.Center, "откройте сайт QR.2tip.ru"));
                            qrDoc.Markup.Add(new XElement(Tags.Center, "и введите код официанта:"));
                            string alias = waiterFromDb.Alias.Substring(0, 3) + '-' + waiterFromDb.Alias.Substring(3, 3) + '-' + waiterFromDb.Alias.Substring(6, 3);
                            qrDoc.Markup.Add(new XElement(Tags.Center, alias));
                        }
                        qrDoc.Markup.Add(new XElement(Tags.Center, "Большое спасибо!"));
                        qrDoc.Markup.Add(new XElement(Tags.Center, "2tip.ru"));
                    }
                }
            }
            catch (Exception e)
            {
                PluginContext.Log.Error($"Cheque printing, orderId = {orderId}", e);
            }
            return qrDoc;
        }
        private string UpdateKeyNumber()
        {
            string keyNumberDB = DbManager.GetKeyNumber();
            string keyNumber = Engine.ShowKeyboard("Изменение ключа доступа", keyNumberDB);
            if (keyNumber != null)
            {
               DbManager.UpdateKeyNumber(keyNumber);
            }
            return keyNumber;
        }

        public Plugin()
        {
            this.DbManager = new DBManager(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "2tip"));
            this.client = new RestApi();
            var connectionString = ConfigurationManager.ConnectionStrings["WaitersDBConnectionString"];
            AppDomain.CurrentDomain.SetData("DataDirectory", Environment.CurrentDirectory);
            keyNumber = DbManager.GetKeyNumber();
            if (keyNumber == null)
            {
               DbManager.CreateKeyNumber();
            }

            Button SyncButton = new Button("2Tip.ru Синхронизация", new Action<IViewManager, IReceiptPrinter>((vm, rp) => syncButtonAction(vm, rp)));
            PluginContext.Integration.AddButton(SyncButton);

            Button ChangeKeyNumber = new Button("2Tip.ru Изменение ключа доступа", new Action<IViewManager, IReceiptPrinter>((vm, rp) => {
                bool repeat = true;
                while (repeat)
                {
                    string keyNumber = UpdateKeyNumber();
                    if (keyNumber == null)
                    {
                        break;
                    }
                    var response = JsonConvert.DeserializeObject<Response>(client.SendGet("iiko/validate", keyNumber, string.Empty).Content);
                    if (response == null)
                    {
                        repeat = vm.ShowYesNoPopup("Ошибка соединения с сервером 2tip", "Повторить попытку?");
                        PluginContext.Log.Error("Can't connect to 2tip.ru");
                        continue;
                    }
                    if (response.success != true)
                    {
                        vm.ShowErrorPopup("Неверный ключ доступа");
                    }
                }
            }));
            PluginContext.Integration.AddButton(ChangeKeyNumber);

            PluginContext.Notifications.UserChanged.Subscribe(new Observers.UserObserver(DbManager));
            PluginContext.Notifications.SubscribeOnCashChequePrinting(new RemotableFunc<Guid, CashCheque>(new Func<Guid, CashCheque>((orderId) => {
                PluginContext.Log.Info($"Cash cheque printing, orderId = {orderId}");
                CashCheque cheque = new CashCheque();
                var qrDoc = GetQrDoc(orderId);
                cheque.AfterCheque = qrDoc;
                return cheque;
            })));
            PluginContext.Notifications.SubscribeOnBillChequePrinting(new RemotableFunc<Guid, BillCheque>(new Func<Guid, BillCheque>((orderId) => {
                PluginContext.Log.Info($"Bill cheque printing, orderId = {orderId}");
                BillCheque cheque = new BillCheque();
                var qrDoc = GetQrDoc(orderId);
                cheque.BeforeFooter = qrDoc;
                return cheque;
            })));
        }

        public void Dispose()
        {
            DbManager.Dispose();
        }
    }
}
